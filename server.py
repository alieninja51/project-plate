import flask
from flask import request, jsonify
from imageprocessing import ImageProcessor
from PIL import Image
import numpy as np

app = flask.Flask(__name__)
app.config["DEBUG"] = True

ip = ImageProcessor()

@app.route("/api/v1/resources/plateocr", methods=['POST']) # define a rota e metodo
def plate_ocr():

    if 'file' in request.files: # verifica se uma imagem foi enviada

        img = request.files['file']
        
        image = np.array(Image.open(img.stream).convert('RGB'))
        
        ocr_text = ip.process(image)
        
        response_obj = {"ret" : True, "msg" : ocr_text}
      
        return response_obj
        
    else:
    
        response_obj = {"ret" : False, "msg" : "It doesn't have an image"}
        
        return response_obj
 
if __name__ == "__main__":
    app.run(use_reloader=False, host='0.0.0.0', port=8083)  # seta a porta na qual a aplicacao ira rodar 
