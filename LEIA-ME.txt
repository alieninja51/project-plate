1. Sobre os arquivos:

    1.1 O arquivo imageprocessing.py contém a classe com a lógica de identificação e OCR da placa.

    1.2 O arquivo server.py implementa o endpoint com flask.


2. Configuração do ambiente e execução:

    2.1 Os scripts foram testados a partir do python3.6.3

    2.2 Utilizei um virtual environment para configurar o ambiente de execução. Dessa forma, todas as dependências são instaladas nesse ambiente e podemos facilmente transportar essa pre-configuracao para diferentes hosts.
    
    2.3 Para ativar o virtual environment, digitar o seguinte comando:
    
        python3 source venv-plate/bin/activate
        
    2.4 Para iniciar o serviço, após ativar o virtualenvironment, deve-se executar o arquivo server.py:
    
        python3 server.py
        
        
    2.5 O video passo-passo.mkv demonstra os processos acima citados.
        
    OBS: Considerações relevantes: O meu host não tem GPU, portanto, para melhorar o tempo de execução do serviço, é indicado um host com uma GPU (exige a configuração dos drivers e toolkits do CUDA). Mas para testar a ferramenta, deve ser suficiente sem GPU.


3. O server estará disponível através do seguinte endpoint:

    http://localhost:8083/api/v1/resources/plateocr
    
    3.1 O serviço será acessível através do metodo POST:
    
    3.2 Como a requisição enviará uma imagem, a codigicação utilizada deverá ser multipart/form-data (Content-Type : multipart/form-data).
    
    3.3 A imagem será enviada através da key "file".
    
    3.4 As imagens request01.jpg e request02.jpg apresentam um exemplo de requisição utilizando o cliente POSTMAN. 
    
    
4. Sobre a acurácia da abordagem

    Eu fiz alguns testes para testar a acurácia da abordagem. Vi que a abordagem é relativamente simples e ocorre várias confusões, identificando partes da imagem que muitas vezes nem apresentam uma placa.
    
    A pasta "exemplos-errados" contém alguns exemplos de imagens aparentemente óbvias, mas que a abordagem não foi capaz de tratar.
    
    Se a aplicação vai reconhecer imagens a partir de uma câmera estática, acredito ser possível ajustar os parâmetros para que este script funcione. Entretanto, se o objetivo é fazer uma abordagem que funcione para diferentes cenários e a partir de diferentes ângulos, será necessário utilizar outra abordagem mais sofisticada.   
